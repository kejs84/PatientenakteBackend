# PatientenakteBackend



## Information
- Hier finden Sie den reinen Backend-Quellcode für das Wirtschaftsinformatik Projekt 2 der Gruppe L2.   
- Das Repository ist nur für das Lesen gedacht. Für das Ausführen der Azure Functions werden die Zip-Dateien benötigt. 
- Die Geheimnisse und Umgebungsvariablen sind hier aus Sicherheitsgründen nicht aufgelistet.
